var gvis = (function(){
    var root = undefined;
    var _reqId = 0;
    function simple_chart(table, query_str, callback) {
        if (!root){
            throw "Must provide query server root first - gvis.set_root(...)";
        }
        var reqId = reqId++;
        // building the same query the wrapper does
        query_str = encodeURI(query_str).replace('=','%3D');
        var query = new google.visualization.Query (
            root+'/query/'+table+'/?tq='+query_str+'&tqx=reqId%3A'+reqId,
            { sendMethod: 'scriptInjection' }
        );
        query.send(callback);
    }

    function set_root(new_root){
        root = String(new_root);
    }

    return {
        set_root: set_root,
        simple_chart: simple_chart
    }
})();
