import eventlet
eventlet.monkey_patch()

from flask import Flask, request, send_file, send_from_directory

from gvizds.config import Config
from gvizds.datasource import DataSource

def main(datasource):

    app = Flask(__name__)

    @app.route('/')
    def index():
        return send_file('static/html/index.html')

    @app.route('/static/<path:path>')
    def send_static(path):
        return send_from_directory('static', path)

    @app.route('/query/<table>/')
    def query(table):
        tq = request.args.get('tq')
        tqx = request.args.get('tqx')
        return datasource.query(table, tq, tqx)

    app.run()

if __name__ == '__main__':
    import sys
    filename = sys.argv[1] if len(sys.argv) > 1 else 'config.json'
    config = Config(filename)
    ds = DataSource(config)
    
    main(ds)
