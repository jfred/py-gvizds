import datetime


def fix(field_type, value):
    if field_type == 'date':
        # assume string of format 'yyyy-mm-dd'
        return datetime.datetime.strptime(value, '%Y-%m-%d')
    if field_type == 'datetime':
        # assume string of format 'yyyy-mm-dd hh:mm:ss'
        return datetime.datetime.strptime(value, '%Y-%m-%d %H:%M:%S')
    return value
