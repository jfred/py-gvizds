from gvizds.parser import Query

from nose.tools import eq_


def _compare_sql(q_str, expected_sql):
    sql = Query(q_str).sql('test_table')
    eq_(sql, expected_sql)


def test_all_columns():
    _compare_sql(
        "select *",
        "select * from test_table"
    )


def test_just_columns():
    _compare_sql(
        "select something, else, more_other",
        "select something, else, more_other from test_table"
    )


def test_aggregation_functions_columns():
    _compare_sql(
        "select something, ave(else)",
        "select something, ave(else) as else from test_table"
    )
    _compare_sql(
        "select something, sum(else)",
        "select something, sum(else) as else from test_table"
    )
    _compare_sql(
        "select something, count(else)",
        "select something, count(else) as else from test_table"
    )
    _compare_sql(
        "select something, min(else)",
        "select something, min(else) as else from test_table"
    )
    _compare_sql(
        "select something, max(else)",
        "select something, max(else) as else from test_table"
    )


def test_simple_where():
    _compare_sql(
        "select something, else where a = 1",
        "select something, else from test_table where a = 1"
    )


def test_where_quoted():
    _compare_sql(
        "select something, else where a = '1'",
        "select something, else from test_table where a = '1'"
    )


def test_where_gt():
    _compare_sql(
        "select reportdate, sum(events) where reportdate > '2011-01-01' group by reportdate",
        "select reportdate, sum(events) as events from test_table where reportdate > '2011-01-01' group by reportdate"
    )


def test_simple_where_in():
    _compare_sql(
        "select something, else where a in (1, 2)",
        "select something, else from test_table where a in ( 1, 2 )"
    )


def test_simple_where_not_in():
    _compare_sql(
        "select something, else where a not in (1, 2)",
        "select something, else from test_table where a not in ( 1, 2 )"
    )


def test_simple_where_in_quoted():
    _compare_sql(
        "select something, else where a in ('123','abc')",
        "select something, else from test_table where a in ( '123', 'abc' )"
    )


def test_and_where():
    _compare_sql(
        "select something, else where a = 1 and b = 2",
        "select something, else from test_table where a = 1 and b = 2"
    )


def test_or_where():
    _compare_sql(
        "select something, else where a = 1 or b = 2",
        "select something, else from test_table where a = 1 or b = 2"
    )


def test_date_where():
    _compare_sql(
        "select something, else where a = 2011-01-01",
        "select something, else from test_table where a = 2011-01-01"
    )


def test_group_by():
    _compare_sql(
        "select something, else group by something",
        "select something, else from test_table group by something"
    )


def test_group_by_with_where():
    _compare_sql(
        "select something, else where a = 111 group by something",
        "select something, else from test_table where a = 111 group by something"
    )


def test_pivot():
    _compare_sql(
        "select something, else pivot category",
        "select category, something, else from test_table group by category"
    )


def test_pivot_with_where():
    _compare_sql(
        "select something, else where a = 1 pivot category",
        "select category, something, else from test_table where a = 1 group by category"
    )


def test_pivot_with_group_by():
    _compare_sql(
        "select something, else group by something pivot category",
        "select category, something, else from test_table group by category, something"
    )


def test_pivot_with_where_and_group_by():
    _compare_sql(
        "select something, else where a = 1 group by something pivot category",
        "select category, something, else from test_table where a = 1 group by category, something"
    )
